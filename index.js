console.log("Hello World!")

// 3. & 4.
function numbers(no1,no2){

	if (typeof no1 === "number" && typeof no2 === "number")	{	

		if((no1 + no2) <= 10){
			console.log(`The sum of the numbers : ${no1 + no2}`);
			console.warn("The numbers sums up to  9 or less.")
		}
		else if((no1 + no2) >= 10 && (no1 + no2) <= 20){
			console.log(`The difference of the numbers : ${no1 - no2}`)
			alert("The numbers equals to 10 or greater.")
		}
		else if((no1 + no2) >= 21 && (no1 + no2) <= 29){
			console.log(`The product of the numbers : ${no1 * no2}`)
			alert("The numbers equals to 10 or greater.")
		}
		else if((no1 + no2) >= 30 ){
			console.log(`The quotient of the numbers : ${no1 / no2}`)
			alert("The numbers equals to 10 or greater.")
		}
		} else {
		alert("Invalid input, enter a valid number.") }
}

let no1 = parseInt(prompt("Enter the first number: "));
let no2 = parseInt(prompt("Enter the second number: "));
numbers(no1,no2);

// 5. details
function userDetails(name, age){	
		if(name === null || age === null) {
			alert("Are you a time-traveler?")}		
		else if (typeof name === "string" && typeof age === "number") {
			alert(`Hello ${name}. Your age is ${age}`);
		}
		else {
		alert("Invalid input, enter a valid number.") }
}

let name = prompt("Enter your name:");
let age = parseInt(prompt("Enter your age:"));
userDetails(name, age);

// 6. isLegalAge
function isLegalAge(age1){
	if (age1 < 18){
		alert("You are not allowed here.")
	} else if (age1 >= 18) {
		alert("You are of legal age")
	} else {
		alert("Invalid input, enter a valid number.") }
}

let age1 = parseInt(prompt("What is your age?"));
isLegalAge(age1);


// 7. Switch
let userAge = parseInt(prompt("Input your age?"));
switch (userAge) {
	case 18:
		alert("You are now allowed to party");
		break;
	case 21:
		alert("You are now part of the adult society");
		break;
	case 65:
		alert("We thank you for your contribution to society");
		break;
	default:
		alert("Are you sure you're not an alien?");
		break;
	}

// 8. try
// Create a try catch finally statement to force an error, print the error message as a warning and use the function isLegalAge to print alert another message.

try {
        console.logs((isLegalAge(age1)));
        
    } 
    catch (error) {
    	console.log(typeof error)
        console.warn(error.message);
    }
    finally {
        alert('Check your inputs!');
    }

